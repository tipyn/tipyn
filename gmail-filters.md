# How to set up a few handy filters

## Calendar notifications

1. Go to gmail > settings > filters and blocked addresses
2. Click "new filter" 
3. In the "has the words field, paste the following: `(*.ics AND has:attachment AND ("Invitation:" OR "Accepted" OR "Updated" OR "Declined"))`
4. Press `Continue` (do not press `Search` or you'll have to start over)
5. It will get upset with special characters and get upset, ignore it and click `Confirm`
6. Select the following: `Skip the inbox`; `Apply the label...` (create new label and call it "Calendar Notifications" or whatever you want; `Never send it to Spam`; `Also apply filter to matching conversations`
7. Click `Update filter`

## Filtering notifications from GitLab

1. If you want to follow a project, go to the project and click the little notification bell icon and select watch or participate (or whatver makes sense for you)
2. Go to gmail > settings > filter and blocked addresses
3. In the "has the words" field, type, for example: `list:gitlab-org/fun-project` (so if you wanted to follow: https://gitlab.com/gitlab-org/ux-research/, you would type: `list:gitlab-org/ux-research`)
4. Press `Continue` (do not press `Search` or you'll have to start over)
5. Select the following: `Skip the inbox`; `Apply the label...` (create new label and call it whatever you want; `Never send it to Spam`; `Also apply filter to matching conversations`
7. Click `Update filter`

I recommend filtering the main gitlab-org so that any random notifications on issues don't fill up your inbox. You can do this easily by setting up a quick filter for any notifications that come from `gitlab@mg.gitlab.com` to skip your inbox and go straight to a label. Don't forget to select `apply to all matching conversations` to help you tidy what's already in your inbox!

Enjoy your cleaner and more sane inbox 😊
